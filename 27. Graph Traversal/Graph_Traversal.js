class Graph {
    constructor() {
        this.adjacencyList = {}
    }

    addVertex(vertex) {
        if (!this.adjacencyList[vertex])
            this.adjacencyList[vertex] = [];
    }

    addEdge(vertex1, vertex2) {
        this.adjacencyList[vertex1].push(vertex2);
        this.adjacencyList[vertex2].push(vertex1);
    }

    removeEdge(vertex1, vertex2) {
        this.adjacencyList[vertex1] = this.adjacencyList[vertex1].filter(v=>v !== vertex2);
        this.adjacencyList[vertex2] = this.adjacencyList[vertex2].filter(v=>v !== vertex1);
    }

    removeVertex(vertex) {
        // while (this.adjacencyList[vertex].length) { const adjacentVertex = this.adjacencyList[vertex].pop(); this.removeEdge(vertex, adjacentVertex)}
        for (let adjacentVertex of this.adjacencyList[vertex]) {
            this.removeEdge(vertex, adjacentVertex)
        }
        delete this.adjacencyList[vertex];
    }

    DFS_recursive(start) {
        const result = [];
        const visited = {};
        const adjacencyList = this.adjacencyList;

        function dfs(vertex) {
            if (!vertex)
                return null;
            visited[vertex] = true;
            result.push(vertex);
            adjacencyList[vertex].forEach(neighbor=>{
                if (!visited[neighbor]) {
                    return dfs(neighbor);
                }
            }
            );
        }
        dfs(start);
        return result;
    }

    DFS_Iterative(start) {
        const stack = [];
        // stack = [start]
        const result = [];
        const visited = {};
        stack.push(start);
        // no need if we set the start in stack itself
        visited[start] = true;
        while (stack.length) {
            var curr = stack.pop();
            result.push(curr);
            this.adjacencyList[curr].forEach(neighbor=>{
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    stack.push(neighbor);
                }
            }
            );
        }
        return result;
    }

    BFS(start) {
        const queue = [start];
        const result = [];
        const visited = {};
        let currentVertex;
        visited[start] = true;
        while (queue.length) {
            currentVertex = queue.shift();
            result.push(currentVertex);
            this.adjacencyList[currentVertex].forEach(neighbor=>{
                // this.adjacencyList[currentVertex].slice().reverse().forEach(neighbor =>
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    queue.push(neighbor);
                }
            }
            );
        }
        return result;
    }

}

let g = new Graph();
g.addVertex("A");
g.addVertex("B");
g.addVertex("C");
g.addVertex("D");
g.addEdge("C", "B")
g.addEdge("A", "B")
g.addEdge("C", "A")
g.addEdge("D", "A")
// g.DFS_recursive("A");
// g.DFS_Iterative("A");
g.BFS("A");

// Node - val, next, prev

class Node {
    constructor(val) {
        this.val = val;
        this.next = null;
        this.prev = null;
    }
}

class DoublyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    push(val) {
        let newNode = new Node(val);
        if (this.length == 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;
        }
        this.length++;
        return this;
    }

    pop() {
        if (this.length === 0)
            return undefined;
        let poppedTail = this.tail;
        if (this.length === 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.tail = poppedTail.prev;
            this.tail.next = null;
            poppedTail.prev = null;
        }
        this.length--;
        return poppedTail;
    }

    shift() {
        if (this.length === 0)
            return undefined;
        let oldHead = this.head;
        if (this.length === 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.head = oldHead.next;
            this.head.prev = null;
            oldHead.next = null;
        }
        this.length--;
        return oldHead;
    }

    unShift(val) {
        let newNode = new Node(val);
        if (this.length === 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.head.prev = newNode;
            newNode.next = this.head;
            this.head = newNode;
        }
        this.length++;
        return this;
    }

    get(index) {
        if (index < 0 || index >= this.length)
            return null;
        if (index <= (this.length / 2)) {
            let count = 0;
            let current = this.head;
            while (count !== index) {
                current = current.next;
                count++;
            }
            return current;
        } else {
            let count = this.length - 1;
            let current = this.tail;
            while (count !== index) {
                current = current.prev;
                count--;
            }
            return current;
        }
    }

    set(index, val) {
        let node = this.get(index);
        if (node !== null) {
            node.val = val;
            return true;
        }
        return false;
    }

    insert(index, val) {
        if (index < 0 || index > this.length)
            return false;
        if (index === 0)
            return !!this.unShift(val);
        if (index === this.length)
            return !!this.push(val);
        let newNode = new Node(val);
        let prevNode = this.get(index - 1);
        let afterNode = prevNode.next;
        prevNode.next = newNode;
        newNode.prev = prevNode;
        newNode.next = afterNode;
        afterNode.prev = newNode;
        this.length++;
        return true;
    }

    remove(index) {
        if (index < 0 || index >= this.length)
            return undefined;
        if (index === 0)
            return !!this.shift();
        if (index === this.length - 1)
            return !!this.pop();
        let remNode = this.get(index);
        let prevNode = this.get(index - 1);
        let afterNode = this.get(index + 1);
        prevNode.next = afterNode;
        afterNode.prev = prevNode;
        remNode.next = null;
        remNode.prev = null;
        this.length--;
        return remNode;
    }

    reverse2() {
        list.print();
        if (this.head === null)
            return null;
        let current = this.head;
        this.tail = current;
        while (current) {
            let temp = current.prev;
            current.prev = current.next;
            current.next = temp;
            if (current.prev) {
                current = current.prev;
            } else {
                this.head = current;
                return this;
            }
        }
    }

    reverse() {
        let currNode = this.head;
        let tail = this.head;
        let nextNode = null;

        while (currNode) {
            nextNode = currNode.next;
            currNode.next = currNode.prev;
            currNode.prev = nextNode;
            if (!nextNode) {
                this.tail = tail;
                this.head = currNode;
            }
            currNode = nextNode;
        }
        return this;
    }

    print() {
        let arr = [];
        let current = this.head;
        while (current) {
            arr.push(current.val);
            current = current.next;
        }
        console.log(arr);
    }
}

let list = new DoublyLinkedList;
list.push(1)
list.push(2)
list.push(3)
list.push(5)

// list.pop();
list.unShift(44)
// list.insert(0, 88)

list.reverse()
// list.reverse2()

# Using while loop
def add_up_to(n):
    total = 0
    while n > 0:
        total = total + n
        n = n - 1
    return total


total = add_up_to(4)
print(total)


# using mathematical formula
def add_up_to_formula(n):
    return n * (n + 1) / 2


print(add_up_to_formula(4))


# Using for loop
def add_up_to_for_loop(n):
    total = 0
    for i in range(n + 1):
        total += i
    return total


print(add_up_to_for_loop(4))
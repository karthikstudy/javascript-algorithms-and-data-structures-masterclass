def is_prime_number():
    for n in range(10, 20):
        if n > 1:
            for i in range(2, n):
                if n % i == 0:
                    j = int(n / i)
                    print("{} equals {} * {}".format(n, i, j))
                    break
            else:
                print(n, " is a prime number")


# is_prime_number()


def prime_num_up_to(n):
    primes = []
    for y in range(2, n):
        for z in range(2, y):
            if y % z == 0:
                break
        else:
            primes.append(y)
    return primes


# print(prime_num_up_to(100))

def prime_using_while():
    i = 2
    while i < 100:
        j = 2
        while j <= (i / j):
            if not (i % j):
                break
            j = j + 1
        if j > i / j:
            print(i, " is prime")
        i = i + 1


prime_using_while()

function add_up_to(n) {
    total = 0;
    for (let i = 0; i <= n; i++) {
        total = total + i;
    }
    return total;
}

console.log(add_up_to(103));

function addUpTo2(n) {
    return n * (n + 1) / 2;
}

// console.log(addUpTo2(4643753645))
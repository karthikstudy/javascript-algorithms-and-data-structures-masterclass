function validAnagram(str1, str2) {
    if (str1.length !== str2.length) {
        return false;
    }

    let firstCounter = {};
    let secondCounter = {};
    // console.log(str1[1]);
    for (let i = 0; i < str1.length; i++) {
        currentElement = str1[i];
        if (typeof (firstCounter[currentElement]) !== "undefined") {
            firstCounter[currentElement] = firstCounter[currentElement] + 1;
        } else {
            firstCounter[currentElement] = 1;
        }
    }
    console.log(firstCounter);

    for (let i = 0; i < str2.length; i++) {
        currentElement = str2[i];
        if (typeof (secondCounter[currentElement]) !== "undefined") {
            secondCounter[currentElement] = secondCounter[currentElement] + 1;
        } else {
            secondCounter[currentElement] = 1;
        }
    }
    console.log(secondCounter);

    for (i = 0; i < str1.length; i++) {
        if (firstCounter[str1[i]] !== secondCounter[str1[i]]) {
            console.log('N');
            return false;
        }
    }
    console.log('Y');
    return true;
}

// validAnagram("aaz", "zza");

function validAnagram2(str1, str2) {
    if (str1.length !== str2.length) {
        return false;
    }

    let firstCounter = {};
    let secondCounter = {};

    for(let val of str1) {
        firstCounter[val] = (firstCounter[val] || 0) + 1;
    }
    console.log(firstCounter);

    for(let val of str2) {
        secondCounter[val] = (secondCounter[val] || 0) + 1;
    }
    console.log(secondCounter);

    for (let key in str1) {
        if (firstCounter[str1[key]] !== secondCounter[str1[key]]) {
            console.log('N');
            return false;
        }
    }
    console.log('Y');
    return true;
}

validAnagram2("anagram", "nagaram");

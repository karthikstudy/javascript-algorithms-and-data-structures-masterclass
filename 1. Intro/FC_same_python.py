"""
# Frequency Counter Pattern
Write a function called same, which accepts two arrays. The function should return true if every value in the array has it's corresponding value squared in the second array. The frequency of values must be the same.
"""


# naive solution - O(n^2)
def same(arr1, arr2):
    if len(arr1) != len(arr2):
        print("Arrays are not equal")
        return False
    for i in range(len(arr1)):
        current_index = arr1[i] ** 2
        if current_index not in arr2:
            print(current_index, "is not present in second array")
            return False
    print(True)


same([1, 2, 3], [1, 4, 9])


# Refactored Solution - O(n)
def same_refactored(arr1, arr2):
    if len(arr1) != len(arr2):
        return False
    frequency_counter = {}
    frequency_counter2 = {}
    for i in arr1:
        if i in frequency_counter:
            frequency_counter[i] += 1
        else:
            frequency_counter[i] = 1
    for j in arr2:
        if j in frequency_counter2:
            frequency_counter2[j] += 1
        else:
            frequency_counter2[j] = 1
    print(frequency_counter)
    print(frequency_counter2)

    for key in frequency_counter:
        if key ** 2 not in frequency_counter2:
            print(key ** 2, "is not present in second array")
            return False
        if frequency_counter2[key ** 2] != frequency_counter[key]:
            print("Both arrays have different frequency")
            return False
    print(True)
    return True


same_refactored([1, 2, 3], [1, 4, 9])

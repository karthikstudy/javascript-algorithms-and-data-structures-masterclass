"""
# Frequency Counter Pattern
Given two strings, write a function to determine if the second string is an anagram of the first. An anagram is a word, phrase, or name formed by rearranging the letters of another, such as cinema, formed from iceman.
"""


def anagram_refactored(str1, str2):
    if len(str1) != len(str2):
        print("String lengths are not equal")
        return False

    frequency_counter = {}
    frequency_counter2 = {}

    for i in str1:
        if i in frequency_counter:
            frequency_counter[i] += 1
        else:
            frequency_counter[i] = 1
    print(frequency_counter)

    for j in str2:
        if j in frequency_counter2:
            frequency_counter2[j] += 1
        else:
            frequency_counter2[j] = 1
    print(frequency_counter2)

    for key in frequency_counter:
        # if key not in frequency_counter2:
        #     print(key, "character is not present in second string")
        #     return False
        if frequency_counter[key] != frequency_counter2[key]:
            print("Frequencies mismatch")
            return False
    print(True)
    return True


anagram_refactored("karthik", "kihtrak")

def count_up(n):
    for i in range(n):
        print(i)


count_up(5)


def count_down(n):
    for j in reversed(range(n)):  # range(n, -1, -1)
        print(j)


count_down(5)

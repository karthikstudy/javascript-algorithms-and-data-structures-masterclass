function searchString(long, short) {
    let count = 0;
    for (let i = 0; i < long.length; i++) {
        for (let j = 0; j < short.length; j++) {
            // console.log(short[j], long[i + j]);
            if (short[j] !== long[i + j]) break;
            // console.log(j,short.length-1);
            if (j === short.length - 1) count++;
        }
    }
    return count;
}
//recursive
function strCount (str, sub) {
    var subLen = sub.length;
    var strLen = str.length;
    if (strLen < subLen) {
        return 0;
    } else if (str.slice(0, subLen) === sub) {
        return 1 + strCount(str.substring(subLen), sub);
    } else return strCount(str.substring(1), sub);
}

// strCount("qwe asd zxcasd as d", "asd");

console.log(searchString("qwe asd zxcasd as d", "asd"));
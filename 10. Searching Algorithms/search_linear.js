function linearSearch(arr, num) {
    for(let i in arr) {
        if(arr[i] === num) return i;
    }
    return -1;
}

console.log(linearSearch([1,2,3,4,5], 3));
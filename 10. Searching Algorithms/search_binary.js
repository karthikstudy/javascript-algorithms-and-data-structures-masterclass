function binarySearch(arr, val) {
    let start = 0;
    let end = arr.length - 1;

    while (start <= end) {
        let middle = Math.floor((start + end) / 2);
        if (arr[middle] === val) {
            return middle;
        } else if (val > arr[middle]) {
            start = middle + 1;
        } else {
            end = middle - 1;
        }
    }
    return -1;
}

//another moethod
function binarySearch2(arr, val) {
    let start = 0;
    let end = arr.length - 1;
    let middle = Math.floor((start + end) / 2);

    while (arr[middle] !== val && start <= end) {
        if (val < arr[middle]) end = middle - 1;
        else start = middle + 1;
        middle = Math.floor((start + end) / 2);
    }
    return arr[middle] === val ? middle : -1;
}

// console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 12));
console.log(binarySearch2([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 8));
// Write a recursive function called fib which accepts a number and returns the
// nth number in the Fibonacci sequence. Recall that the Fibonacci sequence is
// the sequence of whole numbers 1,1,2,3,5,8, ... which starts with 1 and 1, and
// where every number thereafter is equal to the sum of the previous two numbers


// fib(4) // 3
// fib(10) // 55
// fib(28) // 317811
// fib(35) // 9227465

function fib(num) {
    let fib = [0,1];
    for (let i = 2; i <= num; i++) {
       fib[i] = fib[i-2] + fib[i-1];
    //    console.log(fib[i]);
    //    console.log(fib);
    }
    return fib[fib.length-1];
}

function fib1(num) {
    return num <= 2 ? 1 : fib(num - 2) + fib(num - 1);
}

console.log(fib1(3));
function pivot(arr, start = 0, end = arr.length + 1) {
    function swap(arr, i, j) {
        let temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    let pivot = arr[start];
    let swapIdx = 0;
    for (let i = start + 1; i < arr.length; i++) {
        if (pivot > arr[i]) {
            swapIdx++;
            swap(arr, i, swapIdx);
        }
    }
    swap(arr, swapIdx, start);
    console.log(arr);
    return swapIdx;
}

console.log(pivot([6, 4, 8, 2, 1, 5, 7, 9, 3]));
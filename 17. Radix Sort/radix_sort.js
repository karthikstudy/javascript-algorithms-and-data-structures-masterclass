function getDigit(num, i) {
    return Math.floor(Math.abs(num) / Math.pow(10, i) % 10);
}

function digitCount(num) {
    if (num === 0) return 1;
    return Math.floor(Math.log10(Math.abs(num))) + 1;
}

function mostDigits(nums) {
    let maxDigit = 0;
    for (let i = 0; i < nums.length; i++) {
        maxDigit = Math.max(maxDigit, digitCount(nums[i]));
    }
    return maxDigit;
}

function radixSort(nums) {
    let count = mostDigits(nums);
    for (let i = 0; i < count; i++) {
        let digitBuckets = Array.from({length: 10}, () => []);
        for (let j = 0; j < nums.length; j++) {
            let digit = getDigit(nums[j], i);
            digitBuckets[digit].push(nums[j]);
        }
        nums = [].concat(...digitBuckets);
    }
    return nums;
}

console.log(radixSort([34, 3242, 7, 123, 8798, 24]));
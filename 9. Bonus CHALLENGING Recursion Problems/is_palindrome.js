// Write a recursive function called isPalindrome which returns true if the
// string passed to it is a palindrome. Otherwise it returns false

// isPalindrome('awesome') // false
// isPalindrome('foobar') // false
// isPalindrome('tacocat') // true
// isPalindrome('amanaplanacanalpanama') // true
// isPalindrome('amanaplanacanalpandemonium') // false

function isPalindrome(str) {
    function reverse(string) {
        return string.length === 0 ? "" : reverse(string.slice(1)) + string[0];
    }
    let reversed = reverse(str);
    return reversed === str;
}

function isPalindrome2(str) {
    if (str.length === 1) return 1;
    if (str.length === 2) return str[0] === str[1];
    if (str[0] === str.slice(-1)) return isPalindrome2(str.slice(1,-1));
    return false;
}

console.log(isPalindrome2('amanaplanacanalpanama'));
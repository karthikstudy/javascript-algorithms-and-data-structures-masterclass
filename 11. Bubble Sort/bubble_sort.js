function bubbleSrot(arr) {
    let noSwaps;
    for (let i = arr.length; i > 0; i--) {
        noSwaps = true;
        for (let j = 0; j < i - 1; j++) {
            // console.log(arr, arr[j], arr[j + 1]);
            if (arr[j] > arr[j + 1]) {
                // console.log("SWAP");
                let temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
                noSwaps = false;
            }
        }
        if (noSwaps) break;
        // console.log( i + " Pass Complete!");
    }
    return arr;
}

console.log(bubbleSrot([5, 3, 8, 56, 34, 12, -3, 58]));
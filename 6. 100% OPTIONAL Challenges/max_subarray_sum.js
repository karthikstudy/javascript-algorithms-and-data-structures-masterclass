/*** Sliding Window - maxSubarraySum ***/

/*
Given an array of integers and a number, write a function called maxSubArraySum,
which finds the maximum sum of a subarray with the length of the number passed
to the function. Note that a subarray consist of consecutive elements from the 
original array. In the first example below, [100, 200, 300] is a subarray of the
original array, but [100,300] is not.

 maxSubarraySum([100,200,300,400], 2) // 700
 maxSubarraySum([1,4,2,10,23,3,1,0,20], 4) // 39
 maxSubarraySum([-3,4,0,-2,6,-1], 2) // 5
 maxSubarraySum([3,-2,7,-4,1,-1,4,-2,1], 2) // 5
 maxSubarraySum([2,3], num) // null

 Constraints:
 Time Complexity - O(N)
 Space Complexity - O(1)
*/

// Naive Solution - O(N^2)
function maxSubarraySum2(arr, num) {
    if (num > arr.length) return null;
    let max = 0;
    for (let i = 0; i < arr.length - num + 1; i++) {
        let temp = 0;
        for (let j = i; j < i + num; j++) {
            temp += arr[j];
        }
        if (temp > max) {
            max = temp;
        }
    }
    return max;
}

// Refactored - O(N)
function maxSubarraySum(arr, num) {
    let temp = 0;
    let max = 0;
    for (let i = 0; i < num; i++) {
        max += arr[i];
    }
    let temp = max;
    for (let j = num; j < arr.length; j++) {
        temp = temp - arr[j - num] + arr[j];
        if (temp > max) {
            max = temp;
        }
    }
    return max;
}
maxSubarraySum([100, 200, 300, 400], 2);
// maxSubarraySum([1,4,2,10,23,3,1,0,20], 4)
// maxSubarraySum([-3,4,0,-2,6,-1], 2) // 5
// maxSubarraySum([3,-2,7,-4,1,-1,4,-2,1], 2)
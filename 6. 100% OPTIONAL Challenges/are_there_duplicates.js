/*** Coding Exercise 4: Frequency  Counter / Multiple Pointers - areThereDuplicates ***/

/*
Q: Implement a function called, areThereDuplicates which accepts a variable number of arguments, and
checks whether there are any duplicates among the arguments passed in.
You can solve this using the Frequency Counter pattern OR the Multiple Pointers pattern.


areThereDuplicates([1, 2, 3]); //false
areThereDuplicates([1, 2, 2]); //true
areThereDuplicates('a', 'b', 'c', 'a'); //false

Restictions:
    Time - O(N)
    Space - O(N)

Bonus:
    Time - O(N LOG N)
    Space - O(1)

*/

//Using Frequency Counter Pattern
function areThereDuplicates(...arguments) {
    let counter = {};
    for (let val of arguments) {
        counter[val] = (counter[val] || 0) + 1;
        if (counter[val] > 1) {
            return true;
        }
    }
    return false;
}

//Using Multiple Pointers Pattern

function areThereDuplicates2(...arguments) {
    if (typeof arguments[0] === 'number') {
        arguments.sort(function (a, b) {
            return a - b; //for sorting numbers
        });
    } else if (typeof arguments[0] === 'string') {
        arguments.sort(); //for sorting alphabets
    } else {
        return false;
    }

    let start = 0;
    let next = 1;
    while (next < arguments.length) {
        if (arguments[start] === arguments[next]) {
            return true;
        }
        start++;
        next++;
    }
    return false;
}

// Using Set
function areThereDuplicates3() {
    return new Set(arguments).size !== arguments.length;
}

// areThereDuplicates2(1, 2, 3, 1);
// areThereDuplicates2('a', 'b', 'c', 'a');
areThereDuplicates2( 1, 2, 3);
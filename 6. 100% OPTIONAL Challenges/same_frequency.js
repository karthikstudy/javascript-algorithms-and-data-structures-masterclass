/***    Frequency Counter    ***/ 

// Write a function called sameFrequency
// Given two positive integers, find out if the two
// numbers have the same frequency of digits.

// Your solution MUST have the following complexities:
// Time: O(N)

//  sameFrequency(182, 281) // true
//  sameFrequency(34, 14) // false
//  sameFrequency(3589578, 5879385) // true
//  sameFrequency(22, 222) // false

function sameFrequency(num1, num2) {
    num1 = num1.toString();
    num2 = num2.toString();

    if(num1.length !== num2.length) return false;

    let counter1 = {};
    let counter2 = {};
    for (let val of num1) {
        counter1[val] = (counter1[val] || 0) + 1;
    }
    for (let val of num2) {
        counter2[val] = (counter2[val] || 0) + 1;
    }

    for (let key in counter1) {
        if (counter1[key] !== counter2[key]) {
            return false;
        }
    }
    return true;
    
}

// sameFrequency(182, 281) // true
  sameFrequency(34, 31) // false

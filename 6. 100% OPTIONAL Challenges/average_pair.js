/*** Multiple Pointers - averagePair ***/

/*
 Write a function called averagePair. Given a sorted array of integers and a target average, 
 determine if there is a pair of values in the array where the average of the pair equals the 
 target average. There may be more than one pair that matches the average

 Bonus constraints:
 Time: O(N)
 Space: O(1)
 Sample Input:
      averagePair([1,2,3], 2.5); // true
      averagePair([1,3,3,5,6,7,10,12,19], 8); // true
      averagePair([-1,0,3,4,5,6], 4.1); // false
      averagePair([], 4); // false
*/

// Naive Solution - O(N^2)
function averagePair(arr, val) {
      let next = 1;
      let count = {};
      for (let i = 0; i < arr.length; i++) {
            for (let j = i + 1; j < arr.length; j++) {
                  var avg = (arr[i] + arr[j]) / 2;
                  if (avg === val) {
                        count["index " + next] = [i, j];
                        count["pair " + next] = [arr[i], arr[j]];
                        next++;
                  }
            }
      }
      console.log(count);
      return count;
}

// Refactored - O(N)
function averagePairRefactored(arr, val) {
      let left = 0;
      let right = arr.length - 1;
      let next = 1;
      let count = {};
      while (left < right) {
            let avg = (arr[left] + arr[right]) / 2;
            if (avg > val) {
                  right--;
            }else if (avg < val) {
                  left++;
            }else {
                  count["index " + next] = [left, right];
                  count["pair " + next] = [arr[left], arr[right]];
                  next++;
                  left++;
                  right--;
            }
      }
      console.log(count);
      return count;
}

// averagePairRefactored([1,2,3], 2.5); // true
averagePairRefactored([1,3,3,5,6,7,10,12,19], 8); // true
// averagePairRefactored([-1,0,3,4,5,6], 4.1); // false
// averagePairRefactored([], 4); // false
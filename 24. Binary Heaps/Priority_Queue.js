class Node {
    constructor(val, priority=0) {
        this.value = val;
        this.priority = priority;
    }
}

// Same as MinBinaryHeap
class PriorityQueue {
    constructor() {
        this.values = [];
    }

    enqueue(value, priority) {
        let newNode = new Node(value, priority);
        this.values.push(newNode);
        this.bubbleUp();
        return this;
    }

    bubbleUp() {
        let index = this.values.length - 1;
        const element = this.values[index];
        while (index > 0) {
            let parentIndex = Math.floor((index - 1) / 2);
            let parent = this.values[parentIndex];
            if (element.priority >= parent.priority)
                break;
            this.values[parentIndex] = element;
            this.values[index] = parent;
            index = parentIndex;
        }

    }

    dequeue() {
        let min = this.values[0];
        let end = this.values.pop();
        if (this.values.length > 0) {
            this.values[0] = end;
            this.sinkDown();
        }
        return min;
    }

    sinkDown() {
        let index = 0;
        let length = this.values.length;
        let element = this.values[0];
        while (true) {
            let leftIndex = 2 * index + 1;
            let rightIndex = 2 * index + 2;
            let leftChild, rightChild;
            let swap = null;

            if (leftIndex < length) {
                leftChild = this.values[leftIndex];
                if (leftChild.priority < element.priority) {
                    swap = leftIndex;
                }
            }

            if (rightIndex < length) {
                rightChild = this.values[rightIndex];
                if ((swap === null && rightChild.priority < element.priority) || (swap !== null && rightChild.priority < leftChild.priority)) {
                    swap = rightIndex;
                }
            }

            if (swap === null)
                break;
            this.values[index] = this.values[swap];
            this.values[swap] = element;
            index = swap;
        }
    }

}

let ER = new PriorityQueue();
ER.enqueue("Common Cold", 5);
ER.enqueue("Gunshot Wound", 1);
ER.enqueue("High fever", 4);
ER.enqueue("Broken Arm", 2);
ER.enqueue("Maleria", 1);
// heap.extractMax();

class MaxBinaryHeap {
    constructor() {
        this.values = [55, 41, 39, 33, 18, 27, 12];
    }

    insert(element) {
        this.values.push(element);
        this.bubbleUp();
        return this;
    }

    bubbleUp() {
        let index = this.values.length - 1;
        const element = this.values[index];
        while (index > 0) {
            let parentIndex = Math.floor((index - 1) / 2);
            let parent = this.values[parentIndex];
            if (element <= parent)
                break;
            this.values[parentIndex] = element;
            this.values[index] = parent;
            index = parentIndex;
        }

    }

    //my Implementation - very verbose
    swap(arr, i, j) {
        let temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    extractMaxVerbose() {
        let max = this.values[0];
        let end = this.values.pop();
        if (this.values.length > 0) {
            this.values[0] = end;
            let index = 0;
            let element = this.values[index];
            while (true) {
                let leftIndex = Math.floor((2 * index) + 1);
                let rightIndex = Math.floor((2 * index) + 2);
                let leftChild = this.values[leftIndex];
                let rightChild = this.values[rightIndex];

                //             if (leftChild > element && rightChild < element) {
                //                 this.swap(this.values, leftIndex, index);
                //                 index = leftIndex;
                //             } else if (leftChild > element && rightChild < element) {
                //                 this.swap(this.values, rightIndex, index);
                //                 index = rightIndex;
                //             } else 

                if (leftChild > element && rightChild > element) {
                    if (leftChild > rightChild) {
                        this.swap(this.values, leftIndex, index);
                        index = leftIndex;
                    } else {
                        this.swap(this.values, rightIndex, index);
                        index = rightIndex;
                    }
                } else {
                    return this.values;
                }
            }
            return this.values;
        }
    }

    // neat Implementation

    extractMax() {
        let max = this.values[0];
        let end = this.values.pop();
        if (this.values.length > 0) {
            this.values[0] = end;
            this.sinkDown();
        }
        return max;
    }

    sinkDown() {
        let index = 0;
        let length = this.values.length;
        let element = this.values[0];
        while (true) {
            let leftIndex = 2 * index + 1;
            let rightIndex = 2 * index + 2;
            let leftChild, rightChild;
            let swap = null;

            if (leftIndex < length) {
                leftChild = this.values[leftIndex];
                if (leftChild > element) {
                    swap = leftIndex;
                }
            }

            if (rightIndex < length) {
                rightChild = this.values[rightIndex];
                if ((swap === null && rightChild > element) || (swap !== null && rightChild > leftChild)) {
                    swap = rightIndex;
                }
            }

            if (swap === null)
                break;
            this.values[index] = this.values[swap];
            this.values[swap] = element;
            index = swap;
        }
    }

}

let heap = new MaxBinaryHeap();
// heap.insert(55);
heap.extractMax();

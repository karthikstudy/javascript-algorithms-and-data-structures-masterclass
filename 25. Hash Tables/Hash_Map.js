class HashTable {
    constructor(size=13) {
        this.keyMap = new Array(size);
    }

    _hash(key) {
        let total = 0;
        let prime = 31;
        for (let i = 0; i < Math.min(key.length, 100); i++) {
            let char = key[i];
            let value = char.charCodeAt(0) - 96;
            total = (total * prime + value) % this.keyMap.length;
            //             console.log(total);
        }
        return total;
    }

    set(key, value) {
        let index = this._hash(key);
        if (!this.keyMap[index]) {
            this.keyMap[index] = [];
        }
        this.keyMap[index].push([key, value]);
        return index;
    }

    get(key) {
        let index = this._hash(key);
        if (this.keyMap[index]) {
            for (let element of this.keyMap[index]) {
                if (element[0] === key) {
                    return element[1];
                }
            }
        }
        return undefined;
    }

    keys() {
        let keys = [];
        for (let element of this.keyMap) {
            if (element) {
                for (let sub of element) {
                    if (!values.includes(sub[1])) {
                        keys.push(sub[0]);
                    }
                }
            }
        }
        return keys;
    }

    values() {
        let values = [];
        for (let element of this.keyMap) {
            if (element) {
                for (let sub of element) {
                    if (!values.includes(sub[1])) {
                        values.push(sub[1]);
                    }
                }
            }
        }
        return values;
    }
}

let ht = new HashTable();
ht.set("maroon", "#800000")
ht.set("yellow", "#FFFF00")
ht.set("olive", "#808000")
ht.set("salmon", "#FA8072")
ht.set("lightcoral", "#F08080")
ht.set("mediumvioletred", "#C71585")
ht.set("plum", "#DDA0DD")
ht.set("purple", "#DDA0DD")
ht.set("voilet", "#DDA0DD")

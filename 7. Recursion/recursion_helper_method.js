function countOdds(arr) {
    let result = [];

    function helper(helperInput) {
        if(helperInput.length === 0) {
            return;
        }
        if(helperInput[0] % 2 !== 0) {
            result.push(helperInput[0]);
        }
        helper(helperInput.slice(1));
    }
    helper(arr);
    return result;
}

function countOdds2(arr) {
    let result = [];
    for(let val of arr) {
        if(val % 2 !== 0) {
            result.push(val);
        }
    }
    return result;
}

countOdds2([1,2,3,4,5,6,7,8,9]);
countOdds([1,2,3,4,5,6,7,8,9]);